Rails.application.routes.draw do
  
  devise_for :admins, controllers: {
      sessions: 'admins/sessions'
      }
    
  devise_for :usuarios, controllers: {
        sessions: 'usuarios/sessions'
      }
    
  resources :zombies do
      resources :brains
  end
    
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    
    root to: 'zombies#index'
    #root to: 'admins#sign_up'
    
    devise_scope :admin do
        #get = ruta que se quiera escribir
        get "admins/usuarios/list",
        to: "admins/sessions#list",
        as: "usuarios_list"
    end
    
end

