class AddApellidoToAdmins < ActiveRecord::Migration[5.0]
  def change
    add_column :admins, :apellido, :string
  end
end
