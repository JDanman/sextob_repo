class AddUsuarioToZombie < ActiveRecord::Migration[5.0]
  def change
    add_reference :zombies, :usuario, foreign_key: true
  end
end
