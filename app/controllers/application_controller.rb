class ApplicationController < ActionController::Base
              
    #protect_from_forgery with: :exception
    #before_action :authenticate_usuario!
    #before_action :configure_permited_parameters, if: :devise_controller?
    
    protect_from_forgery with: :exception
        devise_group :person, contains: [:usuario, :admin]
        before_action :authenticate_person!
        before_action :configure_permited_parameters, if: :devise_controller?

    protected
    
    def configure_permited_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:rol, :nombre, :apellido])
    end
    
     #Redireccionar dependiendo de admin o usuario
		def after_sign_path_for(resource)
			if current_admin		
				usuarios_list_path                
			else
				zombies_path
			end
		end
    
end
